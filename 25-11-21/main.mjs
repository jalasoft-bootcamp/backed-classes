import { assert } from "chai";
import { f } from "./export.mjs";

f();

import * as externalModule from "./export.mjs";

externalModule.foo();

import {square as sq} from './lib/my-math.mjs';