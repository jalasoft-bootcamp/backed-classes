export function f() {
  console.log('f() called');
}

export const one = 1;

export { foo, b as bar };

function foo() {
  console.log('foo() called');
}

function b() {
  console.log('b() called');
}