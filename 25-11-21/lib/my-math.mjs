export { square };

function square(num) {
  return Math.sqrt(num);
}