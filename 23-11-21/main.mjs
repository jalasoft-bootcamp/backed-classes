import { assert } from "chai";

// Binary (base 2)
assert.equal(0b11, 3);

// Octal (base 8)
assert.equal(0o10, 8);

// Decimal 
assert.equal(3, 3);

// Hex 
assert.equal(0xE7, 231);


let x = 0;
const result = [x++, x];

assert.equal(result, [1, 0]);


