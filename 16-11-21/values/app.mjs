import { assert } from "chai"

let str = 'abc';

// assert.equal(str.length, 3);
// str.length = 1;

// passed by value
let x = 123;
let y = x;
x = 5;
// assert.equal(x, y);
assert.equal(123 === 123, true);
assert.equal('abc' === 'abc', true);

// objects mutable by default

const obj = {};
obj.count = 2;

assert.equal(obj.count, 2);

obj.count = 3;

assert.equal(obj.count, 3);

// Passed by identity
const a = {};
const b = a;

assert.equal(a === b, true);

a.name = 'teresa';

assert.equal(b.name, 'teresa');
