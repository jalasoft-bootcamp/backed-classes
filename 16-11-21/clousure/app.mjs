
import { assert, expect } from "chai"

// assert.throws(() => new MyClass(), ReferenceError);

// class MyClass { }

// assert.equal(() => new MyClass instanceof MyClass, true);


// Partial early activation:
function f() {
  assert.equal(x, undefined);

  if (true) {

    let x = 123;
    assert.equal(x, 123);
  }

  assert.equal(x, 123);
}

// f();

// Bound varialbes vs free variables
function func(x) {
  const y = 123;
  console.log(z);
}

// Clousure, func + connection to the variables + the place where born the variable
function funcFactory(value) {
  return () => {
    return value;
  }
}

let myVar = 'a';
{
  console.log(myVar);
  let myVar;
  myVar = 'b';
}