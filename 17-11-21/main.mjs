import { assert } from "chai"

// Compared by identity

// const obj = {}

// assert.equal(obj === obj, true);
// assert.equal({} === {}, false);


// Object are compared by identity
// Primitives
// if (typeof x === 'string') {
  
// }
//Objects
// if (x instanceof Array) {
  
// }

// console.log(typeof String);

// assert.equal(Number('123'), 123);
// assert.equal(new Number('123').valueOf(), 123);


// ------------------------------------------------

const obj = {}
obj['true'] = 123;

// Coerce true to the string 'true'
assert.equal(obj[true], 123);


const sum = [1, 2, 3] + [4, 5, 6];
String([1,2,3])

const myString = 'There are ' + 3 + ' items'