import { assert } from "chai";

//Cohesion
const sum = 5 + true;

let a = 3;

// console.log(a **= 2);

// console.log(a **= 0);

// Bitwise and Assignment

// 0101
let b = 5;
// 0011
b &= 3;

console.log(b);

console.log(5 & 3);

// Bitwise or Assignment

let c = 5;
c |= 3;

console.log(c);

console.log(10 | 5);


// Logical or assignment ||=

const myObject = { duration: 50, title: false }

myObject.duration ||= 10;

console.log(myObject.duration);


// Logical and assignment &&=

let x = 1;
let y = 0;

x &&= 2;
console.log(x);

y &&= 2;
console.log(y);


// Logical nullish assignment

myObject.duration ??= 100;

console.log(myObject.duration);

myObject.speed ??= 200;
console.log(myObject.speed);

// nullish coalescencia

const foo = undefined ?? 'default string';

console.log(foo);

const baz = 0 ?? 50;
console.log(baz);

// Optional chaining

const adventure = {
  name: 'Alice',
  cat: {
    name: 'dina'
  }
};

console.log(adventure.name.dog?.name);

console.log(adventure.name.someNonExistingFuntion?.().name.test);

