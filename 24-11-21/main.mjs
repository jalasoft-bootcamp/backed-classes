// import { assert } from "chai";

BigInt.prototype.toJSON = function () { return this.toString() + 'n' }

// console.log(JSON.stringify(BigInt(1234567890n)));

const myObj = {
  testNumber: 123,
  testBigInt: 12345678901345678901234567890n
}


console.log(JSON.stringify(myObj));



const props = Object.keys(myObj);


// for (let index = 0; index < props.length; index++) {
//   if (typeof myObj.props[index] === 'bigint') {
//     console.log('asdfasdf');
//   }
// }

for (const item in myObj) {
  if (typeof myObj[item] === 'bigint') {
    console.log('is bigint');
  }
}

const myJson = '{ "testNumber": 123, "testBigInt": "987n"}';
const backAgain = JSON.parse(myJson, (key, value) => {
  if (typeof value === "string" && /^\d+n$/.test(value)) {
    return BigInt(value.substr(0, value.length - 1));
  }
  return value;
});

console.log(backAgain);