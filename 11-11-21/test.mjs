import { expect } from "chai"

const numbers = [1, 2, 3, 4, 5];

describe('my first test', function () {
  
  this.beforeEach(function () {
    
  })

  it('shoud add numbers', function () {
    expect(10 + 5).to.equal(15);
    expect(Array.isArray(numbers)).to.equal(true);
    expect(numbers).to.be.an('array');
    expect(numbers).to.include(2);
    expect(numbers.length).to.equal(5);
  })
  
})